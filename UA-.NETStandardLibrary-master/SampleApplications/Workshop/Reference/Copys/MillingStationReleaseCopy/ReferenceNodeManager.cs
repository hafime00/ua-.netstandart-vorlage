﻿/* ========================================================================
 * Copyright (c) 2005-2016 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

using System;
using System.Collections.Generic;
using System.Xml;
using System.Diagnostics;
using System.Threading;
using Opc.Ua;
using Opc.Ua.Server;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Text;
using System.Globalization;
using System.ComponentModel;

namespace Quickstarts.ReferenceServer
{


    /// <summary>
    /// A node manager for a server that exposes several variables.
    /// </summary>
    public class NodeManager : CustomNodeManager2
    {
        #region Private Fields
        private ReferenceServerConfiguration m_configuration;
        private Timer readAndAir;
        private List<BaseDataVariableState> axisVariables;
        private List<BaseDataVariableState> coordinateVariables;
        private List<BaseDataVariableState> speedVariables;
        private List<BaseDataVariableState> miomVariables;
        private List<BaseDataVariableState> miomStaticVariablesName;
        private List<BaseDataVariableState> miomStaticVariablesDescription;
        private List<BaseDataVariableState> miomStaticVariablesReferenceDesignator;
        private List<BaseDataVariableState> miomStaticVariablesAddress;
        private List<BaseDataVariableState> miomStaticVariablesMIOMType;
        private List<BaseDataVariableState> miomVariablesWord;


        private CultureInfo cultureForNumbers = new CultureInfo("en-US");
        private string millingRobotIp = "192.168.62.56";
        private int millingRobotPort = 9001;
        private TcpClient tcpClient1;
        private NetworkStream nwStream1;
        private TcpClient tcpClient2;
        private NetworkStream nwStream2;
        private TcpClient tcpClient3;
        private NetworkStream nwStream3;
        private TcpClient tcpClient4;
        private NetworkStream nwStream4;
        private TcpClient tcpClient5;
        private NetworkStream nwStream5;
        private TcpClient tcpClient6;
        private NetworkStream nwStream6;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes the node manager.
        /// </summary>
        public NodeManager(IServerInternal server, ApplicationConfiguration configuration)
        :
            base(server, configuration, Namespaces.ReferenceApplications)
        {
            SystemContext.NodeIdFactory = this;

            // get the configuration for the node manager.
            m_configuration = configuration.ParseExtension<ReferenceServerConfiguration>();

            // use suitable defaults if no configuration exists.
            if (m_configuration == null)
            {
                m_configuration = new ReferenceServerConfiguration();
            }

            axisVariables = new List<BaseDataVariableState>();
            coordinateVariables = new List<BaseDataVariableState>();
            speedVariables = new List<BaseDataVariableState>();
            miomVariables = new List<BaseDataVariableState>();
            miomVariablesWord = new List<BaseDataVariableState>();
            miomStaticVariablesName = new List<BaseDataVariableState>();
            miomStaticVariablesDescription = new List<BaseDataVariableState>();
            miomStaticVariablesReferenceDesignator = new List<BaseDataVariableState>();
            miomStaticVariablesAddress = new List<BaseDataVariableState>();
            miomStaticVariablesMIOMType = new List<BaseDataVariableState>();
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// An overrideable version of the Dispose.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TBD
            }
        }
        #endregion

        #region INodeIdFactory Members
        /// <summary>
        /// Creates the NodeId for the specified node.
        /// </summary>
        public override NodeId New(ISystemContext context, NodeState node)
        {
            BaseInstanceState instance = node as BaseInstanceState;

            if (instance != null && instance.Parent != null)
            {
                string id = instance.Parent.NodeId.Identifier as string;

                if (id != null)
                {
                    return new NodeId(id + "_" + instance.SymbolicName, instance.Parent.NodeId.NamespaceIndex);
                }
            }

            return node.NodeId;
        }
        #endregion

        #region Private Helper Functions
        private static bool IsUnsignedAnalogType(BuiltInType builtInType)
        {
            if (builtInType == BuiltInType.Byte ||
                builtInType == BuiltInType.UInt16 ||
                builtInType == BuiltInType.UInt32 ||
                builtInType == BuiltInType.UInt64)
            {
                return true;
            }
            return false;
        }

        private static bool IsAnalogType(BuiltInType builtInType)
        {
            switch (builtInType)
            {
                case BuiltInType.Byte:
                case BuiltInType.UInt16:
                case BuiltInType.UInt32:
                case BuiltInType.UInt64:
                case BuiltInType.SByte:
                case BuiltInType.Int16:
                case BuiltInType.Int32:
                case BuiltInType.Int64:
                case BuiltInType.Float:
                case BuiltInType.Double:
                    return true;
            }
            return false;
        }

        private static Opc.Ua.Range GetAnalogRange(BuiltInType builtInType)
        {
            switch (builtInType)
            {
                case BuiltInType.UInt16:
                    return new Range(System.UInt16.MaxValue, System.UInt16.MinValue);
                case BuiltInType.UInt32:
                    return new Range(System.UInt32.MaxValue, System.UInt32.MinValue);
                case BuiltInType.UInt64:
                    return new Range(System.UInt64.MaxValue, System.UInt64.MinValue);
                case BuiltInType.SByte:
                    return new Range(System.SByte.MaxValue, System.SByte.MinValue);
                case BuiltInType.Int16:
                    return new Range(System.Int16.MaxValue, System.Int16.MinValue);
                case BuiltInType.Int32:
                    return new Range(System.Int32.MaxValue, System.Int32.MinValue);
                case BuiltInType.Int64:
                    return new Range(System.Int64.MaxValue, System.Int64.MinValue);
                case BuiltInType.Float:
                    return new Range(System.Single.MaxValue, System.Single.MinValue);
                case BuiltInType.Double:
                    return new Range(System.Double.MaxValue, System.Double.MinValue);
                case BuiltInType.Byte:
                    return new Range(System.Byte.MaxValue, System.Byte.MinValue);
                default:
                    return new Range(System.SByte.MaxValue, System.SByte.MinValue);
            }
        }
        #endregion

        #region INodeManager Members
        /// <summary>
        /// Does any initialization required before the address space can be used.
        /// </summary>
        /// <remarks>
        /// The externalReferences is an out parameter that allows the node manager to link to nodes
        /// in other node managers. For example, the 'Objects' node is managed by the CoreNodeManager and
        /// should have a reference to the root folder node(s) exposed by this node manager.  
        /// </remarks>
        public override void CreateAddressSpace(IDictionary<NodeId, IList<IReference>> externalReferences)
        {
            lock (Lock)
            {
                IList<IReference> references = null;

                if (!externalReferences.TryGetValue(ObjectIds.ObjectsFolder, out references))
                {
                    externalReferences[ObjectIds.ObjectsFolder] = references = new List<IReference>();
                }

                FolderState root = CreateFolder(null, "MELFA", "MELFA");
                root.AddReference(ReferenceTypes.Organizes, true, ObjectIds.ObjectsFolder);
                references.Add(new NodeStateReference(ReferenceTypes.Organizes, false, root.NodeId));
                root.EventNotifier = EventNotifiers.SubscribeToEvents;
                AddRootNotifier(root);

                //Creating Folders here
                FolderState millingRobot = CreateFolder(root, "MillingRobot", "MillingRobot");
                FolderState axisInformation = CreateFolder(millingRobot, "MillingRobot.Axis", "Axis");
                FolderState coordinateInformation = CreateFolder(millingRobot, "MillingRobot.Coordinates", "Coordinates");
                FolderState speedInformation = CreateFolder(millingRobot, "MillingRobot.Speed", "Speed");
                //MIOM Folders
                FolderState miomInformation = CreateFolder(millingRobot, "MillingRobot.MIOM", "MIOM");
                FolderState miomInputsInformation = CreateFolder(miomInformation, "MillingRobot.MIOM.Inputs", "Inputs");
                FolderState miomOutputsInformation = CreateFolder(miomInformation, "MillingRobot.MIOM.Outputs", "Outputs");
                //Folders for inputs and outputs (name, type, description, reference designator, address)
                FolderState miomInputsInformationWord0 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0", "miomElementInputWord0");
                FolderState miomOutputsInformationWord0 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0", "miomElementOutputWord0");
                FolderState miomInputsInformationWordH = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWordH", "miomElementInputWordH");
                FolderState miomOutputsInformationWordH = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWordH", "miomElementOutputWordH");
                FolderState miomInputsInformationWord1 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord1", "miomElementInputWord1");
                FolderState miomOutputsInformationWord1 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord1", "miomElementOutputWord1");

                FolderState miomInputsInformationWord0Bit0 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit0", "miomElementInputWord0Bit0");
                FolderState miomInputsInformationWord0Bit1 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit1", "miomElementInputWord0Bit1");
                FolderState miomInputsInformationWord0Bit2 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit2", "miomElementInputWord0Bit2");
                FolderState miomInputsInformationWord0Bit3 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit3", "miomElementInputWord0Bit3");
                FolderState miomInputsInformationWord0Bit4 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit4", "miomElementInputWord0Bit4");
                FolderState miomInputsInformationWord0Bit5 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit5", "miomElementInputWord0Bit5");
                FolderState miomInputsInformationWord0Bit6 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit6", "miomElementInputWord0Bit6");
                FolderState miomInputsInformationWord0Bit7 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit7", "miomElementInputWord0Bit7");
                FolderState miomInputsInformationWord0Bit8 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit8", "miomElementInputWord0Bit8");
                FolderState miomInputsInformationWord0Bit9 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit9", "miomElementInputWord0Bit9");
                FolderState miomInputsInformationWord0Bit10 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit10", "miomElementInputWord0Bit10");
                FolderState miomInputsInformationWord0Bit11 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit11", "miomElementInputWord0Bit11");
                FolderState miomInputsInformationWord0Bit12 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit12", "miomElementInputWord0Bit12");
                FolderState miomInputsInformationWord0Bit13 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit13", "miomElementInputWord0Bit13");
                FolderState miomInputsInformationWord0Bit14 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit14", "miomElementInputWord0Bit14");
                FolderState miomInputsInformationWord0Bit15 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit15", "miomElementInputWord0Bit15");

                FolderState miomOutputsInformationWord0Bit0 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit0", "miomElementOutputWord0Bit0");
                FolderState miomOutputsInformationWord0Bit1 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit1", "miomElementOutputWord0Bit1");
                FolderState miomOutputsInformationWord0Bit2 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit2", "miomElementOutputWord0Bit2");
                FolderState miomOutputsInformationWord0Bit3 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit3", "miomElementOutputWord0Bit3");
                FolderState miomOutputsInformationWord0Bit4 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit4", "miomElementOutputWord0Bit4");
                FolderState miomOutputsInformationWord0Bit5 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit5", "miomElementOutputWord0Bit5");
                FolderState miomOutputsInformationWord0Bit6 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit6", "miomElementOutputWord0Bit6");
                FolderState miomOutputsInformationWord0Bit7 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit7", "miomElementOutputWord0Bit7");
                FolderState miomOutputsInformationWord0Bit8 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit8", "miomElementOutputWord0Bit8");
                FolderState miomOutputsInformationWord0Bit9 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit9", "miomElementOutputWord0Bit9");
                FolderState miomOutputsInformationWord0Bit10 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit10", "miomElementOutputWord0Bit10");
                FolderState miomOutputsInformationWord0Bit11 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit11", "miomElementOutputWord0Bit11");
                FolderState miomOutputsInformationWord0Bit12 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit12", "miomElementOutputWord0Bit12");
                FolderState miomOutputsInformationWord0Bit13 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit13", "miomElementOutputWord0Bit13");
                FolderState miomOutputsInformationWord0Bit14 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit14", "miomElementOutputWord0Bit14");
                FolderState miomOutputsInformationWord0Bit15 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit15", "miomElementOutputWord0Bit15");
                                                     
                FolderState miomInputsInformationWord1Bit0 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit0",  "miomElementInputWord1Bit0");
                FolderState miomInputsInformationWord1Bit1 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit1",  "miomElementInputWord1Bit1");
                FolderState miomInputsInformationWord1Bit2 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit2",  "miomElementInputWord1Bit2");
                FolderState miomInputsInformationWord1Bit3 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit3",  "miomElementInputWord1Bit3");
                FolderState miomInputsInformationWord1Bit4 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit4",  "miomElementInputWord1Bit4");
                FolderState miomInputsInformationWord1Bit5 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit5",  "miomElementInputWord1Bit5");
                FolderState miomInputsInformationWord1Bit6 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit6",  "miomElementInputWord1Bit6");
                FolderState miomInputsInformationWord1Bit7 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit7",  "miomElementInputWord1Bit7");
                FolderState miomInputsInformationWord1Bit8 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit8",  "miomElementInputWord1Bit8");
                FolderState miomInputsInformationWord1Bit9 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit9",  "miomElementInputWord1Bit9");
                FolderState miomInputsInformationWord1Bit10 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit10", "miomElementInputWord1Bit10");
                FolderState miomInputsInformationWord1Bit11 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit11", "miomElementInputWord1Bit11");
                FolderState miomInputsInformationWord1Bit12 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit12", "miomElementInputWord1Bit12");
                FolderState miomInputsInformationWord1Bit13 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit13", "miomElementInputWord1Bit13");
                FolderState miomInputsInformationWord1Bit14 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit14", "miomElementInputWord1Bit14");
                FolderState miomInputsInformationWord1Bit15 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit15", "miomElementInputWord1Bit15");

                FolderState miomOutputsInformationWord1Bit0 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit0",  "miomElementOutputWord1Bit0");
                FolderState miomOutputsInformationWord1Bit1 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit1",  "miomElementOutputWord1Bit1");
                FolderState miomOutputsInformationWord1Bit2 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit2",  "miomElementOutputWord1Bit2");
                FolderState miomOutputsInformationWord1Bit3 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit3",  "miomElementOutputWord1Bit3");
                FolderState miomOutputsInformationWord1Bit4 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit4",  "miomElementOutputWord1Bit4");
                FolderState miomOutputsInformationWord1Bit5 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit5",  "miomElementOutputWord1Bit5");
                FolderState miomOutputsInformationWord1Bit6 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit6",  "miomElementOutputWord1Bit6");
                FolderState miomOutputsInformationWord1Bit7 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit7",  "miomElementOutputWord1Bit7");
                FolderState miomOutputsInformationWord1Bit8 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit8",  "miomElementOutputWord1Bit8");
                FolderState miomOutputsInformationWord1Bit9 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit9",  "miomElementOutputWord1Bit9");
                FolderState miomOutputsInformationWord1Bit10 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit10", "miomElementOutputWord1Bit10");
                FolderState miomOutputsInformationWord1Bit11 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit11", "miomElementOutputWord1Bit11");
                FolderState miomOutputsInformationWord1Bit12 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit12", "miomElementOutputWord1Bit12");
                FolderState miomOutputsInformationWord1Bit13 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit13", "miomElementOutputWord1Bit13");
                FolderState miomOutputsInformationWord1Bit14 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit14", "miomElementOutputWord1Bit14");
                FolderState miomOutputsInformationWord1Bit15 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit15", "miomElementOutputWord1Bit15");

                FolderState miomInputsInformationWordHBit0 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit0",  "miomElementInputWordHBit0");
                FolderState miomInputsInformationWordHBit1 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit1",  "miomElementInputWordHBit1");
                FolderState miomInputsInformationWordHBit2 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit2",  "miomElementInputWordHBit2");
                FolderState miomInputsInformationWordHBit3 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit3",  "miomElementInputWordHBit3");
                FolderState miomInputsInformationWordHBit4 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit4",  "miomElementInputWordHBit4");
                FolderState miomInputsInformationWordHBit5 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit5",  "miomElementInputWordHBit5");
                FolderState miomInputsInformationWordHBit6 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit6",  "miomElementInputWordHBit6");
                FolderState miomInputsInformationWordHBit7 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit7",  "miomElementInputWordHBit7");
                FolderState miomInputsInformationWordHBit8 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit8",  "miomElementInputWordHBit8");
                FolderState miomInputsInformationWordHBit9 = CreateFolder(miomInputsInformation,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit9",  "miomElementInputWordHBit9");
                FolderState miomInputsInformationWordHBit10 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit10", "miomElementInputWordHBit10");
                FolderState miomInputsInformationWordHBit11 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit11", "miomElementInputWordHBit11");
                FolderState miomInputsInformationWordHBit12 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit12", "miomElementInputWordHBit12");
                FolderState miomInputsInformationWordHBit13 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit13", "miomElementInputWordHBit13");
                FolderState miomInputsInformationWordHBit14 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit14", "miomElementInputWordHBit14");
                FolderState miomInputsInformationWordHBit15 = CreateFolder(miomInputsInformation, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit15", "miomElementInputWordHBit15");

                FolderState miomOutputsInformationWordHBit0 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit0",  "miomElementOutputWordHBit0");
                FolderState miomOutputsInformationWordHBit1 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit1",  "miomElementOutputWordHBit1");
                FolderState miomOutputsInformationWordHBit2 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit2",  "miomElementOutputWordHBit2");
                FolderState miomOutputsInformationWordHBit3 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit3",  "miomElementOutputWordHBit3");
                FolderState miomOutputsInformationWordHBit4 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit4",  "miomElementOutputWordHBit4");
                FolderState miomOutputsInformationWordHBit5 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit5",  "miomElementOutputWordHBit5");
                FolderState miomOutputsInformationWordHBit6 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit6",  "miomElementOutputWordHBit6");
                FolderState miomOutputsInformationWordHBit7 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit7",  "miomElementOutputWordHBit7");
                FolderState miomOutputsInformationWordHBit8 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit8",  "miomElementOutputWordHBit8");
                FolderState miomOutputsInformationWordHBit9 = CreateFolder(miomOutputsInformation,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit9",  "miomElementOutputWordHBit9");
                FolderState miomOutputsInformationWordHBit10 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit10", "miomElementOutputWordHBit10");
                FolderState miomOutputsInformationWordHBit11 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit11", "miomElementOutputWordHBit11");
                FolderState miomOutputsInformationWordHBit12 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit12", "miomElementOutputWordHBit12");
                FolderState miomOutputsInformationWordHBit13 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit13", "miomElementOutputWordHBit13");
                FolderState miomOutputsInformationWordHBit14 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit14", "miomElementOutputWordHBit14");
                FolderState miomOutputsInformationWordHBit15 = CreateFolder(miomOutputsInformation, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit15", "miomElementOutputWordHBit15");

                try
                {
                    #region MillingRobot_Variables
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "MillingRobot.Axis.J1", "J1", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "MillingRobot.Axis.J2", "J2", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "MillingRobot.Axis.J3", "J3", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "MillingRobot.Axis.J4", "J4", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "MillingRobot.Axis.J5", "J5", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "MillingRobot.Axis.J6", "J6", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "MillingRobot.Axis.J7", "J7", DataTypeIds.Double, ValueRanks.Scalar));

                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "MillingRobot.Coordinates.X", "X", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "MillingRobot.Coordinates.Y", "Y", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "MillingRobot.Coordinates.Z", "Z", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "MillingRobot.Coordinates.A", "A", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "MillingRobot.Coordinates.B", "B", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "MillingRobot.Coordinates.C", "C", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "MillingRobot.Coordinates.L1", "L1", DataTypeIds.Double, ValueRanks.Scalar));

                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J1_actual", "J1_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J1_maximum", "J1_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J1_setpoint", "J1_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J2_actual", "J2_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J2_maximum", "J2_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J2_setpoint", "J2_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J3_actual", "J3_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J3_maximum", "J3_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J3_setpoint", "J3_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J4_actual", "J4_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J4_maximum", "J4_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J4_setpoint", "J4_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J5_actual", "J5_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J5_maximum", "J5_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J5_setpoint", "J5_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J6_actual", "J6_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J6_maximum", "J6_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J6_setpoint", "J6_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J7_actual", "J7_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J7_maximum", "J7_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "MillingRobot.Axis.J7_setpoint", "J7_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));

                    //Dynamische Variablen der MIOM Schnittstelle (Values)
                    miomVariablesWord.Add(CreateDynamicVariable(miomInputsInformationWord0, "MillingRobot.MIOM.Inputs.miomElementInputWord0.Value", "Value", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomVariablesWord.Add(CreateDynamicVariable(miomOutputsInformationWord0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0.Value", "Value", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomVariablesWord.Add(CreateDynamicVariable(miomInputsInformationWordH, "MillingRobot.MIOM.Inputs.miomElementInputWordH.Value", "Value", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomVariablesWord.Add(CreateDynamicVariable(miomOutputsInformationWordH, "MillingRobot.MIOM.Outputs.miomElementOutputWordH.Value", "Value", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomVariablesWord.Add(CreateDynamicVariable(miomInputsInformationWord1, "MillingRobot.MIOM.Inputs.miomElementInputWord1.Value", "Value", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomVariablesWord.Add(CreateDynamicVariable(miomOutputsInformationWord1, "MillingRobot.MIOM.Outputs.miomElementOutputWord1.Value", "Value", DataTypeIds.UInt16, ValueRanks.Scalar));

                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit0, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit0.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit1, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit1.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit2, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit2.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit3, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit3.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit4, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit4.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit5, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit5.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit6, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit6.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit7, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit7.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit8, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit8.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit9, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit9.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit10.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit11.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit12.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit13.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit14.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord0Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit15.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));

                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit0.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit1, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit1.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit2, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit2.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit3, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit3.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit4, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit4.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit5, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit5.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit6, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit6.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit7, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit7.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit8, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit8.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit9, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit9.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit10.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit11.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit12.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit13.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit14.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord0Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit15.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));

                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit0,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit0.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit1,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit1.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit2,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit2.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit3,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit3.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit4,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit4.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit5,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit5.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit6,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit6.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit7,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit7.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit8,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit8.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit9,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit9.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit10, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit10.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit11, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit11.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit12, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit12.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit13, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit13.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit14, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit14.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWordHBit15, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit15.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));

                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit0.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit1.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit2.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit3.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit4.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit5.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit6.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit7.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit8.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit9.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit10, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit10.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit11, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit11.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit12, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit12.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit13, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit13.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit14, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit14.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWordHBit15, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit15.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));

                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit0,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit0.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit1,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit1.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit2,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit2.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit3,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit3.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit4,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit4.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit5,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit5.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit6,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit6.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit7,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit7.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit8,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit8.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit9,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit9.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit10.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit11.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit12.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit13.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit14.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomInputsInformationWord1Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit15.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));

                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit0.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit1.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit2.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit3.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit4.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit5.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit6.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit7.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit8.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit9.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit10.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit11.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit12.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit13.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit14.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));
                    miomVariables.Add(CreateDynamicVariable(miomOutputsInformationWord1Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit15.Value", "Value", DataTypeIds.Boolean, ValueRanks.Scalar));

                    //Statische Variablen der MIOM Schnittstelle(name, MiomType, description, reference designator, address) 
                    //index (0-5)
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0, "MillingRobot.MIOM.Inputs.miomElementInputWord0.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordH, "MillingRobot.MIOM.Inputs.miomElementInputWordH.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordH, "MillingRobot.MIOM.Outputs.miomElementOutputWordH.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1, "MillingRobot.MIOM.Inputs.miomElementInputWord1.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord1, "MillingRobot.MIOM.Outputs.miomElementOutputWord1.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    //index (6-21)
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit0, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit0.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit1, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit1.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit2, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit2.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit3, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit3.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit4, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit4.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit5, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit5.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit6, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit6.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit7, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit7.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit8, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit8.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit9, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit9.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit10.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit11.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit12.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit13.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit14.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord0Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit15.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    // index (22 - 37)
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit0.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit1, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit1.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit2, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit2.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit3, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit3.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit4, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit4.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit5, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit5.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit6, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit6.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit7, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit7.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit8, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit8.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit9, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit9.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit10.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit11.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit12.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit13.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit14.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWord0Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit15.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    //index (38 - 53) 
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit0,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit0.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit1,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit1.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit2,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit2.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit3,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit3.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit4,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit4.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit5,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit5.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit6,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit6.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit7,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit7.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit8,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit8.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit9,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit9.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit10, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit10.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit11, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit11.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit12, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit12.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit13, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit13.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit14, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit14.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWordHBit15, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit15.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    // index (54 - 69)
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit0.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit1.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit2.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit3.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit4.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit5.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit6.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit7.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit8.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit9.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit10, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit10.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit11, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit11.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit12, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit12.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit13, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit13.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit14, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit14.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit15, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit15.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    //index (70 - 85) 
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit0,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit0.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit1,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit1.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit2,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit2.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit3,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit3.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit4,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit4.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit5,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit5.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit6,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit6.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit7,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit7.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit8,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit8.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit9,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit9.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit10.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit11.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit12.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit13.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit14.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomInputsInformationWord1Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit15.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    // index (86 - 101)
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit0.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit1.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit2.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit3.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit4.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit5.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit6.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit7.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit8.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit9.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit10.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit11.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit12.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit13.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit14.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesName.Add(CreateVariable(miomOutputsInformationWordHBit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit15.Name", "Name", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0, "MillingRobot.MIOM.Inputs.miomElementInputWord0.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordH, "MillingRobot.MIOM.Inputs.miomElementInputWordH.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordH, "MillingRobot.MIOM.Outputs.miomElementOutputWordH.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1, "MillingRobot.MIOM.Inputs.miomElementInputWord1.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1, "MillingRobot.MIOM.Outputs.miomElementOutputWord1.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit0, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit0.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit1, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit1.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit2, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit2.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit3, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit3.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit4, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit4.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit5, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit5.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit6, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit6.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit7, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit7.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit8, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit8.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit9, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit9.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit10.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit11.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit12.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit13.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit14.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord0Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit15.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit0.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit1, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit1.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit2, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit2.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit3, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit3.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit4, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit4.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit5, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit5.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit6, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit6.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit7, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit7.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit8, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit8.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit9, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit9.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit10.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit11.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit12.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit13.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit14.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord0Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit15.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit0,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit0.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit1,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit1.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit2,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit2.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit3,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit3.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit4,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit4.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit5,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit5.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit6,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit6.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit7,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit7.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit8,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit8.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit9,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit9.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit10, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit10.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit11, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit11.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit12, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit12.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit13, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit13.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit14, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit14.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWordHBit15, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit15.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit0.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit1.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit2.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit3.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit4.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit5.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit6.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit7.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit8.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit9.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit10, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit10.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit11, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit11.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit12, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit12.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit13, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit13.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit14, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit14.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWordHBit15, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit15.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit0,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit0.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit1,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit1.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit2,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit2.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit3,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit3.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit4,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit4.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit5,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit5.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit6,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit6.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit7,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit7.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit8,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit8.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit9,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit9.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit10.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit11.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit12.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit13.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit14.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomInputsInformationWord1Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit15.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit0.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit1.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit2.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit3.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit4.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit5.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit6.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit7.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit8.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit9.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit10.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit11.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit12.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit13.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit14.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesDescription.Add(CreateVariable(miomOutputsInformationWord1Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit15.Description", "Description", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0, "MillingRobot.MIOM.Inputs.miomElementInputWord0.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordH, "MillingRobot.MIOM.Inputs.miomElementInputWordH.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordH, "MillingRobot.MIOM.Outputs.miomElementOutputWordH.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1, "MillingRobot.MIOM.Inputs.miomElementInputWord1.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1, "MillingRobot.MIOM.Outputs.miomElementOutputWord1.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit0, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit0.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit1, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit1.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit2, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit2.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit3, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit3.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit4, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit4.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit5, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit5.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit6, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit6.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit7, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit7.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit8, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit8.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit9, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit9.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit10.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit11.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit12.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit13.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit14.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord0Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit15.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit0.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit1, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit1.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit2, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit2.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit3, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit3.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit4, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit4.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit5, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit5.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit6, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit6.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit7, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit7.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit8, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit8.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit9, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit9.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit10.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit11.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit12.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit13.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit14.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord0Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit15.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit0,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit0.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit1,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit1.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit2,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit2.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit3,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit3.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit4,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit4.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit5,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit5.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit6,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit6.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit7,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit7.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit8,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit8.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit9,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit9.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit10, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit10.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit11, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit11.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit12, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit12.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit13, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit13.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit14, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit14.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWordHBit15, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit15.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit0.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit1.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit2.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit3.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit4.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit5.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit6.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit7.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit8.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit9.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit10, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit10.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit11, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit11.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit12, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit12.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit13, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit13.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit14, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit14.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWordHBit15, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit15.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit0,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit0.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit1,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit1.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit2,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit2.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit3,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit3.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit4,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit4.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit5,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit5.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit6,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit6.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit7,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit7.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit8,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit8.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit9,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit9.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit10.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit11.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit12.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit13.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit14.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomInputsInformationWord1Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit15.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit0.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit1.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit2.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit3.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit4.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit5.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit6.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit7.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit8.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit9.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit10.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit11.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit12.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit13.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit14.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesReferenceDesignator.Add(CreateVariable(miomOutputsInformationWord1Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit15.ReferenceDesignator", "ReferenceDesignator", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0, "MillingRobot.MIOM.Inputs.miomElementInputWord0.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordH, "MillingRobot.MIOM.Inputs.miomElementInputWordH.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordH, "MillingRobot.MIOM.Outputs.miomElementOutputWordH.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1, "MillingRobot.MIOM.Inputs.miomElementInputWord1.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1, "MillingRobot.MIOM.Outputs.miomElementOutputWord1.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit0,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit0.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit1,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit1.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit2,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit2.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit3,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit3.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit4,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit4.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit5,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit5.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit6,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit6.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit7,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit7.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit8,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit8.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit9,  "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit9.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit10.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit11.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit12.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit13.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit14.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord0Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit15.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit0.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit1.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit2.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit3.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit4.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit5.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit6.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit7.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit8.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit9.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit10.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit11.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit12.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit13.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit14.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord0Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit15.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit0,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit0.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit1,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit1.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit2,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit2.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit3,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit3.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit4,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit4.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit5,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit5.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit6,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit6.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit7,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit7.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit8,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit8.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit9,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit9.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit10, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit10.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit11, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit11.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit12, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit12.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit13, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit13.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit14, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit14.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWordHBit15, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit15.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit0.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit1.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit2.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit3.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit4.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit5.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit6.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit7.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit8.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit9.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit10, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit10.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit11, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit11.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit12, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit12.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit13, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit13.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit14, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit14.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWordHBit15, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit15.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit0,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit0.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit1,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit1.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit2,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit2.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit3,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit3.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit4,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit4.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit5,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit5.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit6,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit6.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit7,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit7.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit8,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit8.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit9,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit9.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit10.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit11.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit12.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit13.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit14.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomInputsInformationWord1Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit15.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit0.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit1.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit2.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit3.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit4.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit5.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit6.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit7.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit8.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit9.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit10.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit11.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit12.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit13.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit14.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));
                    miomStaticVariablesAddress.Add(CreateVariable(miomOutputsInformationWord1Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit15.Address", "Address", DataTypeIds.String, ValueRanks.Scalar));

                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0, "MillingRobot.MIOM.Inputs.miomElementInputWord0.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordH, "MillingRobot.MIOM.Inputs.miomElementInputWordH.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordH, "MillingRobot.MIOM.Outputs.miomElementOutputWordH.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1, "MillingRobot.MIOM.Inputs.miomElementInputWord1.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1, "MillingRobot.MIOM.Outputs.miomElementOutputWord1.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));

                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit0, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit0.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit1, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit1.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit2, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit2.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit3, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit3.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit4, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit4.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit5, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit5.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit6, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit6.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit7, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit7.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit8, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit8.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit9, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit9.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit10.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit11.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit12.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit13.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit14.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord0Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord0Bit15.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));

                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit0, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit0.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit1, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit1.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit2, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit2.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit3, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit3.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit4, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit4.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit5, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit5.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit6, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit6.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit7, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit7.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit8, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit8.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit9, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit9.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit10.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit11.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit12.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit13.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit14.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord0Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord0Bit15.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));

                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit0,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit0.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit1,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit1.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit2,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit2.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit3,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit3.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit4,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit4.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit5,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit5.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit6,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit6.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit7,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit7.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit8,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit8.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit9,  "MillingRobot.MIOM.Inputs.miomElementInputWordHBit9.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit10, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit10.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit11, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit11.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit12, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit12.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit13, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit13.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit14, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit14.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWordHBit15, "MillingRobot.MIOM.Inputs.miomElementInputWordHBit15.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));

                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit0.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit1.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit2.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit3.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit4.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit5.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit6.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit7.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit8.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit9.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit10, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit10.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit11, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit11.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit12, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit12.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit13, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit13.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit14, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit14.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWordHBit15, "MillingRobot.MIOM.Outputs.miomElementOutputWordHBit15.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));

                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit0,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit0.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit1,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit1.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit2,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit2.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit3,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit3.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit4,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit4.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit5,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit5.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit6,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit6.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit7,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit7.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit8,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit8.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit9,  "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit9.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit10, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit10.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit11, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit11.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit12, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit12.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit13, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit13.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit14, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit14.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomInputsInformationWord1Bit15, "MillingRobot.MIOM.Inputs.miomElementInputWord1Bit15.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));

                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit0,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit0.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit1,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit1.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit2,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit2.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit3,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit3.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit4,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit4.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit5,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit5.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit6,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit6.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit7,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit7.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit8,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit8.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit9,  "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit9.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit10, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit10.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit11, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit11.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit12, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit12.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit13, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit13.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit14, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit14.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));
                    miomStaticVariablesMIOMType.Add(CreateVariable(miomOutputsInformationWord1Bit15, "MillingRobot.MIOM.Outputs.miomElementOutputWord1Bit15.MIOMType", "MIOMType", DataTypeIds.UInt16, ValueRanks.Scalar));

                    #endregion
                }
                catch (Exception e)
                {
                    Utils.Trace(e, "Error creating the address space.");
                }

                AddPredefinedNode(SystemContext, root);
                FillStaticVariables();

                CreateTCPClient(millingRobotIp, millingRobotPort, out tcpClient1, out nwStream1);
                CreateTCPClient(millingRobotIp, millingRobotPort, out tcpClient2, out nwStream2);
                CreateTCPClient(millingRobotIp, millingRobotPort, out tcpClient3, out nwStream3);
                CreateTCPClient(millingRobotIp, millingRobotPort, out tcpClient4, out nwStream4);
                CreateTCPClient(millingRobotIp, millingRobotPort, out tcpClient5, out nwStream5);
                CreateTCPClient(millingRobotIp, millingRobotPort, out tcpClient6, out nwStream6);
                readAndAir = new Timer(GetValuesFromMillingRobot, null, 100, 100);

            }
        }
        //Statische Variablen mit werten belegen
        private void FillStaticVariables()
        {
            //Names
            miomStaticVariablesName[0].Value = "InputWord0";
            miomStaticVariablesName[1].Value = "OutputWord0";
            miomStaticVariablesName[2].Value = "InputWordH";
            miomStaticVariablesName[3].Value = "OutputWordH";
            miomStaticVariablesName[4].Value = "InputWord1";
            miomStaticVariablesName[5].Value = "OutputWord1";

            for (int i = 6; i <= 21; i++)
            {
                miomStaticVariablesName[i].Value = "InputWord0Bit" + (i - 6);
            }
            for (int i = 22; i <= 37; i++)
            {
                miomStaticVariablesName[i].Value = "OutputWord0Bit" + (i - 22);
            }
            for (int i = 38; i <= 53; i++)
            {
                miomStaticVariablesName[i].Value = "InputWordHBit" + (i - 38);
            }
            for (int i = 54; i <= 69; i++)
            {
                miomStaticVariablesName[i].Value = "OutputWordHBit" + (i - 54);
            }
            for (int i = 70; i <= 85; i++)
            {
                miomStaticVariablesName[i].Value = "InputWord1Bit" + (i - 70);
            }
            for (int i = 86; i <= 101; i++)
            {
                miomStaticVariablesName[i].Value = "OutputWordHBit" + (i - 86);
            }
            //Description
            miomStaticVariablesDescription[0].Value = "16 Bit der ersten Eingangskarte in einem WORD zusammengefasse";
            miomStaticVariablesDescription[1].Value = "16 Bit der ersten Ausgangskarte in einem WORD zusammengefasse";
            miomStaticVariablesDescription[2].Value = "16 Bit der internen Eingangskarte in einem WORD zusammengefasse";
            miomStaticVariablesDescription[3].Value = "16 Bit der internen Ausgangskarte in einem WORD zusammengefasse";
            miomStaticVariablesDescription[4].Value = "16 Bit der zweiten Eingangskarte in einem WORD zusammengefasse";
            miomStaticVariablesDescription[5].Value = "16 Bit der zweiten Ausgangskarte in einem WORD zusammengefasse";

            for (int i = 6; i <= 21; i++)
            {
                miomStaticVariablesDescription[i].Value = "Bit " + (i - 6) + " der ersten Einagngskarte ";
            }
            for (int i = 22; i <= 37; i++)
            {
                miomStaticVariablesDescription[i].Value = "Bit " + (i - 22) + " der ersten Einagngskarte ";
            }
            for (int i = 38; i <= 53; i++)
            {
                miomStaticVariablesDescription[i].Value = "Bit " + (i - 38) + " der internen Einagngskarte ";
            }
            for (int i = 54; i <= 69; i++)
            {
                miomStaticVariablesDescription[i].Value = "Bit " + (i - 54) + " der internen Einagngskarte ";
            }
            for (int i = 70; i <= 85; i++)
            {
                miomStaticVariablesDescription[i].Value = "Bit " + (i - 70) + " der zweiten Einagngskarte ";
            }
            for (int i = 86; i <= 101; i++)
            {
                miomStaticVariablesDescription[i].Value = "Bit " + (i - 86) + " der zweiten Einagngskarte ";
            }
            //ReferenceDesignator
            foreach (BaseDataVariableState bd in miomStaticVariablesReferenceDesignator)
            {
                bd.Value = "BMK = t.b.d.";
            }
            //Address
            miomStaticVariablesAddress[0].Value = "%IW0";
            miomStaticVariablesAddress[1].Value = "%QW0";
            miomStaticVariablesAddress[2].Value = "%IW1";
            miomStaticVariablesAddress[3].Value = "%QW2";
            miomStaticVariablesAddress[4].Value = "%IW1";
            miomStaticVariablesAddress[5].Value = "%QW1";

            for (int i = 6; i <= 21; i++)
            {
                miomStaticVariablesAddress[i].Value = "%IB1" + (i - 6);
            }
            for (int i = 22; i <= 37; i++)
            {
                miomStaticVariablesAddress[i].Value = "%QB1" + (i - 22);
            }
            for (int i = 38; i <= 53; i++)
            {
                miomStaticVariablesAddress[i].Value = "%IB0" + (i - 38);
            }
            for (int i = 54; i <= 69; i++)
            {
                miomStaticVariablesAddress[i].Value = "%QB0" + (i - 54);
            }
            for (int i = 70; i <= 85; i++)
            {
                miomStaticVariablesAddress[i].Value = "%IB2" + (i - 70);
            }
            for (int i = 86; i <= 101; i++)
            {
                miomStaticVariablesAddress[i].Value = "%QB2" + (i - 86);
            }

            //MIOMtype
            foreach (BaseDataVariableState bd in miomStaticVariablesMIOMType)
            {
                bd.Value = 1;
            }

        }

        /// <summary>
        /// Builds TCP connection to Robot and sends command String to get information back
        /// </summary>
        private void GetValuesFromMillingRobot(object data)
        {
            int bufSize = 0;
            string command;
            List<double> returnInformationDoubles;
            List<uint> returnInformationUints;
            List<short> returnInformationMiom;
            List<bool> returnInformationMiomBits;
            string encodedString;
            int bytesRead;
            try
            {
                if (Opc.Ua.Server.Controls.ServerForm.AxisCbChecked)
                {
                    command = "1;1;JPOSF";
                    //Turn text to Bytes
                    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(command);
                    nwStream1.Write(bytesToSend, 0, bytesToSend.Length);
                    //red back Text
                    bufSize = tcpClient1.ReceiveBufferSize;
                    byte[] bytesToRead = new byte[bufSize];
                    bytesRead = nwStream1.Read(bytesToRead, 0, bufSize);
                    returnInformationDoubles = new List<double> { };
                    encodedString = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
                    //Debug.WriteLine(encodedString);
                    returnInformationDoubles = ParseRecivedTextPosition(encodedString);
                    if (returnInformationDoubles != null)
                    {
                        AirInfo<double>(returnInformationDoubles, axisVariables);
                    }
                }
                if (Opc.Ua.Server.Controls.ServerForm.CoordinatesCbChecked)
                {
                    command = "1;1;PPOSF";
                    //Turn text to Bytes
                    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(command);
                    nwStream2.Write(bytesToSend, 0, bytesToSend.Length);
                    //red back Text
                    bufSize = tcpClient2.ReceiveBufferSize;
                    byte[] bytesToRead = new byte[bufSize];
                    bytesRead = nwStream2.Read(bytesToRead, 0, bufSize);
                    returnInformationDoubles = new List<double> { };
                    encodedString = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
                    //Debug.WriteLine(encodedString);
                    returnInformationDoubles = ParseRecivedTextPosition(encodedString);
                    if (returnInformationDoubles != null)
                    {
                        AirInfo<double>(returnInformationDoubles, coordinateVariables);
                    }
                }
                if (Opc.Ua.Server.Controls.ServerForm.SpeedCbChecked)
                {
                    command = "1;1;SRVSPD";
                    //Turn text to Bytes
                    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(command);
                    nwStream3.Write(bytesToSend, 0, bytesToSend.Length);
                    //red back Text
                    bufSize = tcpClient3.ReceiveBufferSize;
                    byte[] bytesToRead = new byte[bufSize];
                    bytesRead = nwStream3.Read(bytesToRead, 0, bufSize);
                    returnInformationUints = new List<uint> { };
                    encodedString = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
                    //Debug.WriteLine(encodedString);
                    returnInformationUints = ParseRecivedText3ColumnTables(encodedString);
                    if (returnInformationUints != null)
                    {
                        AirInfo<uint>(returnInformationUints, speedVariables);
                    }
                }
                if (Opc.Ua.Server.Controls.ServerForm.MiomCbChecked)
                {
                    command = "1;1;IOSIGNAL32;32";
                    //Turn text to Bytes
                    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(command);
                    nwStream4.Write(bytesToSend, 0, bytesToSend.Length);
                    //red back Text
                    bufSize = tcpClient4.ReceiveBufferSize;
                    byte[] bytesToRead = new byte[bufSize];
                    bytesRead = nwStream4.Read(bytesToRead, 0, bufSize);
                    returnInformationMiom = new List<short> { };
                    returnInformationMiomBits = new List<bool> { };
                    encodedString = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
                    //Debug.WriteLine(encodedString);
                    returnInformationMiom = ParseRecivedIOBytesToWords(encodedString);
                    command = "1;1;IOSIGNAL900;900";
                    //Turn text to Bytes
                    byte[] bytesToSend2 = ASCIIEncoding.ASCII.GetBytes(command);
                    nwStream5.Write(bytesToSend2, 0, bytesToSend2.Length);
                    //red back Text
                    bufSize = tcpClient5.ReceiveBufferSize;
                    byte[] bytesToRead2 = new byte[bufSize];
                    bytesRead = nwStream5.Read(bytesToRead2, 0, bufSize);
                    encodedString = Encoding.ASCII.GetString(bytesToRead2, 0, bytesRead);
                    //Debug.WriteLine(encodedString);
                    returnInformationMiom.AddRange (ParseRecivedIOBytesToWords(encodedString));

                    command = "1;1;IOSIGNAL48;48";
                    //Turn text to Bytes
                    byte[] bytesToSend3 = ASCIIEncoding.ASCII.GetBytes(command);
                    nwStream6.Write(bytesToSend3, 0, bytesToSend3.Length);
                    //red back Text
                    bufSize = tcpClient6.ReceiveBufferSize;
                    byte[] bytesToRead3 = new byte[bufSize];
                    bytesRead = nwStream6.Read(bytesToRead3, 0, bufSize);
                    encodedString = Encoding.ASCII.GetString(bytesToRead3, 0, bytesRead);
                    //Debug.WriteLine(encodedString);
                    returnInformationMiom.AddRange(ParseRecivedIOBytesToWords(encodedString));
                    if (returnInformationMiom != null)
                    {
                        AirInfo<short>(returnInformationMiom, miomVariablesWord);
                        returnInformationMiomBits = WordsToBits(returnInformationMiom);
                        AirInfo<bool>(returnInformationMiomBits, miomVariables);
                    }
                }
            }
            catch (Exception e)
            {
                Utils.Trace(e, "Can't get information: ");
            }
        }

        private static void CreateTCPClient(string ip, int port, out TcpClient client, out NetworkStream nwStream)
        {
            //TCP client
            client = new TcpClient(ip, port);
            nwStream = client.GetStream();
        }

        /// <summary>
        /// Puts information in to OPC-UA linked Object
        /// </summary>
        private void AirInfo<T>(List<T> axesInformation, List<BaseDataVariableState> outputPositionList)
        {
            int i = 0;
            foreach (T number in axesInformation)
            {
                outputPositionList[i].Value = number;
                outputPositionList[i].ClearChangeMasks(SystemContext, false);
                i++;
            }
        }

        private List<uint> ParseRecivedText3ColumnTables(string robotAnswer)
        {

            List<uint> speedInformation = new List<uint> { };
            uint outputNumebr = 0;
            try
            {
                //if robot answers with oK   other option: "QeR" Error
                if (robotAnswer.StartsWith("QoK"))
                {
                    robotAnswer = robotAnswer.Remove(0, 3);
                    string[] splitedAnswer = robotAnswer.Split(';');
                    for (int i = 0; i <= 20; i++)
                    {
                        uint.TryParse(splitedAnswer[i], NumberStyles.Any, cultureForNumbers, out outputNumebr);
                        speedInformation.Add(outputNumebr);
                    }
                    return speedInformation;
                }
                else
                {
                    throw new Exception("Robot answers with error");
                }
            }
            catch (Exception e)
            {
                Utils.Trace(e, "3 column parse failed");
                return null;
            }
        }

        private List<double> ParseRecivedTextPosition(string robotAnswer)
        {
            List<double> positionInformation = new List<double> { };
            double outputNumebr = 0;
            try
            {
                //if robot answers with oK   other option: "QeR" Error
                if (robotAnswer.StartsWith("QoK"))
                {
                    string[] splitedAnswer = robotAnswer.Split(';');
                    for (int i = 1; i <= 13; i += 2)
                    {
                        double.TryParse(splitedAnswer[i], NumberStyles.Any, cultureForNumbers, out outputNumebr);
                        positionInformation.Add(outputNumebr);
                    }
                    return positionInformation;
                }
                else
                {
                    throw new Exception("Robot answers with error");
                }
            }
            catch (Exception e)
            {
                Utils.Trace(e, "position parse failed.");
                return null;
            }
        }

        private List<short> ParseRecivedIOBytesToWords(string robotAnswer)
        {
            List<short> ioInformation = new List<short> { };
            string inputWord;
            string outputWord;
            try
            {
                if (robotAnswer.StartsWith("QoK"))
                {
                    inputWord = robotAnswer.Substring(3, 4);
                    outputWord = robotAnswer.Substring(7, 4);
                    //Debug.WriteLine(inputWord + "  " + outputWord);
                    ioInformation.Add(short.Parse(inputWord, NumberStyles.HexNumber));
                    ioInformation.Add(short.Parse(outputWord, NumberStyles.HexNumber));
                }
                else
                {
                    throw new Exception("Robot answers with error");
                }
                return ioInformation;
            }
            catch (Exception e)
            {
                Utils.Trace(e, "IO parse failed");
                return null;
            }
        }
        private List<bool> WordsToBits(List<short> miomWords)
        {
            List<bool> miomBits = new List<bool> { };
            try
            {
                for (int j = 0; j <= 5; j++)
                {
                    for (int i = 0; i <= 15; i++)
                    {
                        miomBits.Add((miomWords[j] & (1 << i)) != 0);
                    }
                }
                return miomBits;
            }
            catch(Exception e)
            {
                Utils.Trace(e, "failed to split Words to bits");
                return null;
            }           
        }

        /// <summary>
        /// Creates a new folder.
        /// </summary>
        private FolderState CreateFolder(NodeState parent, string path, string name)
        {
            FolderState folder = new FolderState(parent);

            folder.SymbolicName = name;
            folder.ReferenceTypeId = ReferenceTypes.Organizes;
            folder.TypeDefinitionId = ObjectTypeIds.FolderType;
            folder.NodeId = new NodeId(path, NamespaceIndex);
            folder.BrowseName = new QualifiedName(path, NamespaceIndex);
            folder.DisplayName = new LocalizedText("en", name);
            folder.WriteMask = AttributeWriteMask.None;
            folder.UserWriteMask = AttributeWriteMask.None;
            folder.EventNotifier = EventNotifiers.None;

            if (parent != null)
            {
                parent.AddChild(folder);
            }

            return folder;
        }

        /// <summary>
        /// Creates a new variable.
        /// </summary>
        private BaseDataVariableState CreateVariable(NodeState parent, string path, string name, NodeId dataType, int valueRank)
        {
            BaseDataVariableState variable = new BaseDataVariableState(parent);

            variable.SymbolicName = name;
            variable.ReferenceTypeId = ReferenceTypes.Organizes;
            variable.TypeDefinitionId = VariableTypeIds.BaseDataVariableType;
            variable.NodeId = new NodeId(path, NamespaceIndex);
            variable.BrowseName = new QualifiedName(path, NamespaceIndex);
            variable.DisplayName = new LocalizedText("en", name);
            variable.WriteMask = AttributeWriteMask.DisplayName | AttributeWriteMask.Description;
            variable.UserWriteMask = AttributeWriteMask.DisplayName | AttributeWriteMask.Description;
            variable.DataType = dataType;
            variable.ValueRank = valueRank;
            variable.AccessLevel = AccessLevels.CurrentReadOrWrite;
            variable.UserAccessLevel = AccessLevels.CurrentReadOrWrite;
            variable.Historizing = false;
            //variable.Value = GetNewValue(variable);  //##HERE
            variable.StatusCode = StatusCodes.Good;
            variable.Timestamp = DateTime.UtcNow;

            if (valueRank == ValueRanks.OneDimension)
            {
                variable.ArrayDimensions = new ReadOnlyList<uint>(new List<uint> { 0 });
            }
            else if (valueRank == ValueRanks.TwoDimensions)
            {
                variable.ArrayDimensions = new ReadOnlyList<uint>(new List<uint> { 0, 0 });
            }

            if (parent != null)
            {
                parent.AddChild(variable);
            }

            return variable;
        }

        /// <summary>
        /// Creates a new variable.
        /// </summary>
        private BaseDataVariableState CreateDynamicVariable(NodeState parent, string path, string name, NodeId dataType, int valueRank)
        {
            BaseDataVariableState variable = CreateVariable(parent, path, name, dataType, valueRank);
            return variable;
        }

        /// <summary>
        /// Frees any resources allocated for the address space.
        /// </summary>
        public override void DeleteAddressSpace()
        {
            lock (Lock)
            {
                // TBD
            }
        }

        /// <summary>
        /// Returns a unique handle for the node.
        /// </summary>
        protected override NodeHandle GetManagerHandle(ServerSystemContext context, NodeId nodeId, IDictionary<NodeId, NodeState> cache)
        {
            lock (Lock)
            {
                // quickly exclude nodes that are not in the namespace. 
                if (!IsNodeIdInNamespace(nodeId))
                {
                    return null;
                }

                NodeState node = null;

                if (!PredefinedNodes.TryGetValue(nodeId, out node))
                {
                    return null;
                }

                NodeHandle handle = new NodeHandle();

                handle.NodeId = nodeId;
                handle.Node = node;
                handle.Validated = true;

                return handle;
            }
        }

        /// <summary>
        /// Verifies that the specified node exists.
        /// </summary>
        protected override NodeState ValidateNode(ServerSystemContext context, NodeHandle handle, IDictionary<NodeId, NodeState> cache)
        {
            // not valid if no root.
            if (handle == null)
            {
                return null;
            }

            // check if previously validated.
            if (handle.Validated)
            {
                return handle.Node;
            }

            // TBD

            return null;
        }
        #endregion

        #region Overrides
        #endregion
    }
}
