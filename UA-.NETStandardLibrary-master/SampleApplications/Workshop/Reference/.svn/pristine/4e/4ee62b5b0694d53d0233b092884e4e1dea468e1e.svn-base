﻿/* ========================================================================
 * Copyright (c) 2005-2016 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

using System;
using System.Collections.Generic;
using System.Xml;
using System.Diagnostics;
using System.Threading;
using Opc.Ua;
using Opc.Ua.Server;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Text;
using System.Globalization;

namespace Quickstarts.ReferenceServer
{
    public class TcpData
    {
        public string ip;
        public int port;
        public string command;
        public List<BaseDataVariableState> outputPositionList;
        public uint parseCase;
        public TcpData(string xip, int xport, string xcommand, List<BaseDataVariableState> xoutputPositionList, uint xparseCase)
        {
            ip = xip;
            port = xport;
            command = xcommand;
            outputPositionList = xoutputPositionList;
            parseCase = xparseCase;
        }
    }

    /// <summary>
    /// A node manager for a server that exposes several variables.
    /// </summary>
    public class NodeManager : CustomNodeManager2
    {
        #region Constructors
        /// <summary>
        /// Initializes the node manager.
        /// </summary>
        public NodeManager(IServerInternal server, ApplicationConfiguration configuration)
        :
            base(server, configuration, Namespaces.ReferenceApplications)
        {
            SystemContext.NodeIdFactory = this;

            // get the configuration for the node manager.
            m_configuration = configuration.ParseExtension<ReferenceServerConfiguration>();

            // use suitable defaults if no configuration exists.
            if (m_configuration == null)
            {
                m_configuration = new ReferenceServerConfiguration();
            }

            axisVariables = new List<BaseDataVariableState>();
            coordinateVariables = new List<BaseDataVariableState>();
            speedVariables = new List<BaseDataVariableState>();
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// An overrideable version of the Dispose.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TBD
            }
        }
        #endregion

        #region INodeIdFactory Members
        /// <summary>
        /// Creates the NodeId for the specified node.
        /// </summary>
        public override NodeId New(ISystemContext context, NodeState node)
        {
            BaseInstanceState instance = node as BaseInstanceState;

            if (instance != null && instance.Parent != null)
            {
                string id = instance.Parent.NodeId.Identifier as string;

                if (id != null)
                {
                    return new NodeId(id + "_" + instance.SymbolicName, instance.Parent.NodeId.NamespaceIndex);
                }
            }

            return node.NodeId;
        }
        #endregion

        #region Private Helper Functions
        private static bool IsUnsignedAnalogType(BuiltInType builtInType)
        {
            if (builtInType == BuiltInType.Byte ||
                builtInType == BuiltInType.UInt16 ||
                builtInType == BuiltInType.UInt32 ||
                builtInType == BuiltInType.UInt64)
            {
                return true;
            }
            return false;
        }

        private static bool IsAnalogType(BuiltInType builtInType)
        {
            switch (builtInType)
            {
                case BuiltInType.Byte:
                case BuiltInType.UInt16:
                case BuiltInType.UInt32:
                case BuiltInType.UInt64:
                case BuiltInType.SByte:
                case BuiltInType.Int16:
                case BuiltInType.Int32:
                case BuiltInType.Int64:
                case BuiltInType.Float:
                case BuiltInType.Double:
                    return true;
            }
            return false;
        }

        private static Opc.Ua.Range GetAnalogRange(BuiltInType builtInType)
        {
            switch (builtInType)
            {
                case BuiltInType.UInt16:
                    return new Range(System.UInt16.MaxValue, System.UInt16.MinValue);
                case BuiltInType.UInt32:
                    return new Range(System.UInt32.MaxValue, System.UInt32.MinValue);
                case BuiltInType.UInt64:
                    return new Range(System.UInt64.MaxValue, System.UInt64.MinValue);
                case BuiltInType.SByte:
                    return new Range(System.SByte.MaxValue, System.SByte.MinValue);
                case BuiltInType.Int16:
                    return new Range(System.Int16.MaxValue, System.Int16.MinValue);
                case BuiltInType.Int32:
                    return new Range(System.Int32.MaxValue, System.Int32.MinValue);
                case BuiltInType.Int64:
                    return new Range(System.Int64.MaxValue, System.Int64.MinValue);
                case BuiltInType.Float:
                    return new Range(System.Single.MaxValue, System.Single.MinValue);
                case BuiltInType.Double:
                    return new Range(System.Double.MaxValue, System.Double.MinValue);
                case BuiltInType.Byte:
                    return new Range(System.Byte.MaxValue, System.Byte.MinValue);
                default:
                    return new Range(System.SByte.MaxValue, System.SByte.MinValue);
            }
        }
        #endregion

        #region INodeManager Members
        /// <summary>
        /// Does any initialization required before the address space can be used.
        /// </summary>
        /// <remarks>
        /// The externalReferences is an out parameter that allows the node manager to link to nodes
        /// in other node managers. For example, the 'Objects' node is managed by the CoreNodeManager and
        /// should have a reference to the root folder node(s) exposed by this node manager.  
        /// </remarks>
        public override void CreateAddressSpace(IDictionary<NodeId, IList<IReference>> externalReferences)
        {
            lock (Lock)
            {
                IList<IReference> references = null;

                if (!externalReferences.TryGetValue(ObjectIds.ObjectsFolder, out references))
                {
                    externalReferences[ObjectIds.ObjectsFolder] = references = new List<IReference>();
                }

                FolderState root = CreateFolder(null, "MELFA", "MELFA");
                root.AddReference(ReferenceTypes.Organizes, true, ObjectIds.ObjectsFolder);
                references.Add(new NodeStateReference(ReferenceTypes.Organizes, false, root.NodeId));
                root.EventNotifier = EventNotifiers.SubscribeToEvents;
                AddRootNotifier(root);

                //Creating Folders here
                FolderState assamblyRobot = CreateFolder(root, "AssamblyRobot", "AsamblyRobot");
                FolderState axisInformation = CreateFolder(assamblyRobot, "AssamblyRobot.Axis", "Axis");
                FolderState coordinateInformation = CreateFolder(assamblyRobot, "AssamblyRobot.Coordinates", "Coordinates");
                FolderState speedInformation = CreateFolder(assamblyRobot, "AssamblyRobot.Speed", "Speed");


                //axisVariables = new List<BaseDataVariableState>();
                //coordinateVariables = new List<BaseDataVariableState>();
                //speedVariables = new List<BaseDataVariableState>();

                try
                {
                    #region AssamblyRobot_Variables
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "AssamblyRobot.Axis.J1", "J1", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "AssamblyRobot.Axis.J2", "J2", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "AssamblyRobot.Axis.J3", "J3", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "AssamblyRobot.Axis.J4", "J4", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "AssamblyRobot.Axis.J5", "J5", DataTypeIds.Double, ValueRanks.Scalar));
                    axisVariables.Add(CreateDynamicVariable(axisInformation, "AssamblyRobot.Axis.J6", "J6", DataTypeIds.Double, ValueRanks.Scalar));

                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "AssamblyRobot.Coordinates.X", "X", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "AssamblyRobot.Coordinates.Y", "Y", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "AssamblyRobot.Coordinates.Z", "Z", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "AssamblyRobot.Coordinates.A", "A", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "AssamblyRobot.Coordinates.B", "B", DataTypeIds.Double, ValueRanks.Scalar));
                    coordinateVariables.Add(CreateDynamicVariable(coordinateInformation, "AssamblyRobot.Coordinates.C", "C", DataTypeIds.Double, ValueRanks.Scalar));

                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J1_actual", "J1_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J1_maximum", "J1_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J1_setpoint", "J1_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J2_actual", "J2_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J2_maximum", "J2_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J2_setpoint", "J2_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J3_actual", "J3_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J3_maximum", "J3_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J3_setpoint", "J3_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J4_actual", "J4_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J4_maximum", "J4_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J4_setpoint", "J4_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J5_actual", "J5_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J5_maximum", "J5_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J5_setpoint", "J5_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J6_actual", "J6_actual", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J6_maximum", "J6_maximum", DataTypeIds.UInt16, ValueRanks.Scalar));
                    speedVariables.Add(CreateDynamicVariable(speedInformation, "AssamblyRobot.Axis.J6_setpoint", "J6_setpoint", DataTypeIds.UInt16, ValueRanks.Scalar));

                    #endregion
                    #region get_AssamblyRobotVars_over_Tcp
                    //Task getAxis = Task.Run(() => GetPositionValuesFromAssamblyRobot("134.108.104.45", 9002, "1;1;JPOSF", axisVariables, 1));
                    //Task getCoordinates = Task.Run(() => GetPositionValuesFromAssamblyRobot("134.108.104.45", 9002, "1;1;PPOSF", coordinateVariables, 1));
                    //Task getSpeed = Task.Run(() => GetPositionValuesFromAssamblyRobot("134.108.104.45", 9002, "1;1;SRVSPD", speedVariables, 2));
                    #endregion
                }
                catch (Exception e)
                {
                    Utils.Trace(e, "Error creating the address space.");
                }

                AddPredefinedNode(SystemContext, root);

                TcpData data1 = new TcpData( "192.168.62.55", 9002, "1;1;JPOSF", axisVariables, 1);
                TcpData data2 = new TcpData( "192.168.62.55", 9002, "1;1;PPOSF", coordinateVariables, 1);
                TcpData data3 = new TcpData( "192.168.62.55", 9002, "1;1;SRVSPD", speedVariables, 2);

                readAndAirJPOSF = new Timer(GetPositionValuesFromAssamblyRobot, data1, 100, 100);
                readAndAirPPOSF = new Timer(GetPositionValuesFromAssamblyRobot, data2, 100, 100);
                readAndAirSRVSPD = new Timer(GetPositionValuesFromAssamblyRobot, data3, 100, 100);
            }
        }

        /// <summary>
        /// Builds TCP connection to Robot and sends command String to get information back
        /// </summary>
        private void GetPositionValuesFromAssamblyRobot(object data)//string ip, int port, string command, List<BaseDataVariableState> outputPositionList, uint parseCase)
        {            
            try
            {
                TcpData innerData = (TcpData)data;
                string ip = innerData.ip;
                int port = innerData.port;
                string command = innerData.command;
                List<BaseDataVariableState> outputPositionList = innerData.outputPositionList;
                uint parseCase = innerData.parseCase;

                TcpClient client;
                NetworkStream nwStream;
                CreateTCPClient(ip, port, out client, out nwStream);
                //while (true)
                //{
                    //Sending Text
                    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(command);
                    nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                    //red back Text
                    byte[] bytesToRead = new byte[client.ReceiveBufferSize];
                    int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
                    switch (parseCase)
                    {
                        case 1:
                            List<double> returnInformation1 = new List<double> { };
                            returnInformation1 = ParseRecivedTextPosition(Encoding.ASCII.GetString(bytesToRead, 0, bytesRead));
                            AirAxisInfo<double>(returnInformation1, outputPositionList);
                            /*Debug.WriteLine(outputPositionList[0].Value + "\n" + 
                                            outputPositionList[1].Value + "\n" +
                                            outputPositionList[2].Value + "\n" +
                                            outputPositionList[3].Value + "\n" +
                                            outputPositionList[4].Value + "\n" +
                                            outputPositionList[5].Value + "\n\n\n");*/
                            break;
                        case 2:
                            List<uint> returnInformation2 = new List<uint> { };
                            returnInformation2 = ParseRecivedText3ColumnTables(Encoding.ASCII.GetString(bytesToRead, 0, bytesRead));
                            AirAxisInfo<uint>(returnInformation2, outputPositionList);
                            break;
                        default:
                            break;
                    }
                    
                //}
            }
            catch (Exception e)
            {
                Utils.Trace(e, "Can't get axis information: ");
            }
        }
        
        private static void CreateTCPClient(string ip, int port, out TcpClient client, out NetworkStream nwStream)
        {
            //TCP client
            client = new TcpClient(ip, port);
            nwStream = client.GetStream();
        }

        /// <summary>
        /// Puts information in to OPC-UA linked Object
        /// </summary>
        private void AirAxisInfo<T>(List<T> axesInformation, List<BaseDataVariableState> outputPositionList)
        {
            int i = 0;
            foreach (T number in axesInformation)
            {
                outputPositionList[i].Value = number;
                outputPositionList[i].ClearChangeMasks(SystemContext, false);
                i++;
            }
            //Debug.WriteLine(outputPositionList[0].Value );
        }

        private List<uint> ParseRecivedText3ColumnTables(string robotAnswer)
        {
            List<uint> speedInformation = new List<uint> { };
            uint outputNumebr = 0;
            //if robot answers with oK   other option: "QeR" Error
            if (robotAnswer.StartsWith("QoK"))
            {
                robotAnswer.Remove(0,3);
                string[] splitedAnswer = robotAnswer.Split(';');
                for (int i = 0; i <= 17; i++)
                {
                    uint.TryParse(splitedAnswer[i], NumberStyles.Any, cultureForNumbers, out outputNumebr);
                    speedInformation.Add(outputNumebr);
                }
            }
            return speedInformation;
        }

        private List<double> ParseRecivedTextPosition(string robotAnswer)
        {
            List<double> positionInformation = new List<double> { };
            double outputNumebr = 0;
            //if robot answers with oK   other option: "QeR" Error
            if (robotAnswer.StartsWith("QoK"))
            {
                string[] splitedAnswer = robotAnswer.Split(';');
                for (int i = 1; i <= 11; i += 2)
                {
                    double.TryParse(splitedAnswer[i], NumberStyles.Any, cultureForNumbers, out outputNumebr);
                    positionInformation.Add(outputNumebr);
                }
            }
            return positionInformation;
        }
        
        /// <summary>
        /// Creates a new folder.
        /// </summary>
        private FolderState CreateFolder(NodeState parent, string path, string name)
        {
            FolderState folder = new FolderState(parent);

            folder.SymbolicName = name;
            folder.ReferenceTypeId = ReferenceTypes.Organizes;
            folder.TypeDefinitionId = ObjectTypeIds.FolderType;
            folder.NodeId = new NodeId(path, NamespaceIndex);
            folder.BrowseName = new QualifiedName(path, NamespaceIndex);
            folder.DisplayName = new LocalizedText("en", name);
            folder.WriteMask = AttributeWriteMask.None;
            folder.UserWriteMask = AttributeWriteMask.None;
            folder.EventNotifier = EventNotifiers.None;

            if (parent != null)
            {
                parent.AddChild(folder);
            }

            return folder;
        }

        /// <summary>
        /// Creates a new variable.
        /// </summary>
        private BaseDataVariableState CreateVariable(NodeState parent, string path, string name, NodeId dataType, int valueRank)
        {
            BaseDataVariableState variable = new BaseDataVariableState(parent);

            variable.SymbolicName = name;
            variable.ReferenceTypeId = ReferenceTypes.Organizes;
            variable.TypeDefinitionId = VariableTypeIds.BaseDataVariableType;
            variable.NodeId = new NodeId(path, NamespaceIndex);
            variable.BrowseName = new QualifiedName(path, NamespaceIndex);
            variable.DisplayName = new LocalizedText("en", name);
            variable.WriteMask = AttributeWriteMask.DisplayName | AttributeWriteMask.Description;
            variable.UserWriteMask = AttributeWriteMask.DisplayName | AttributeWriteMask.Description;
            variable.DataType = dataType;
            variable.ValueRank = valueRank;
            variable.AccessLevel = AccessLevels.CurrentReadOrWrite;
            variable.UserAccessLevel = AccessLevels.CurrentReadOrWrite;
            variable.Historizing = false;
            //variable.Value = GetNewValue(variable);  //##HERE
            variable.StatusCode = StatusCodes.Good;
            variable.Timestamp = DateTime.UtcNow;

            if (valueRank == ValueRanks.OneDimension)
            {
                variable.ArrayDimensions = new ReadOnlyList<uint>(new List<uint> { 0 });
            }
            else if (valueRank == ValueRanks.TwoDimensions)
            {
                variable.ArrayDimensions = new ReadOnlyList<uint>(new List<uint> { 0, 0 });
            }

            if (parent != null)
            {
                parent.AddChild(variable);
            }

            return variable;
        }

        /// <summary>
        /// Creates a new variable.
        /// </summary>
        private BaseDataVariableState CreateDynamicVariable(NodeState parent, string path, string name, NodeId dataType, int valueRank)
        {
            BaseDataVariableState variable = CreateVariable(parent, path, name, dataType, valueRank);
            return variable;
        }
     
        /// <summary>
        /// Frees any resources allocated for the address space.
        /// </summary>
        public override void DeleteAddressSpace()
        {
            lock (Lock)
            {
                // TBD
            }
        }

        /// <summary>
        /// Returns a unique handle for the node.
        /// </summary>
        protected override NodeHandle GetManagerHandle(ServerSystemContext context, NodeId nodeId, IDictionary<NodeId, NodeState> cache)
        {
            lock (Lock)
            {
                // quickly exclude nodes that are not in the namespace. 
                if (!IsNodeIdInNamespace(nodeId))
                {
                    return null;
                }

                NodeState node = null;

                if (!PredefinedNodes.TryGetValue(nodeId, out node))
                {
                    return null;
                }

                NodeHandle handle = new NodeHandle();

                handle.NodeId = nodeId;
                handle.Node = node;
                handle.Validated = true;

                return handle;
            }
        }

        /// <summary>
        /// Verifies that the specified node exists.
        /// </summary>
        protected override NodeState ValidateNode( ServerSystemContext context, NodeHandle handle, IDictionary<NodeId, NodeState> cache)
        {
            // not valid if no root.
            if (handle == null)
            {
                return null;
            }

            // check if previously validated.
            if (handle.Validated)
            {
                return handle.Node;
            }

            // TBD

            return null;
        }
        #endregion

        #region Overrides
        #endregion

        #region Private Fields
        private ReferenceServerConfiguration m_configuration;
        private Opc.Ua.Test.DataGenerator m_generator;
        private Timer readAndAirJPOSF;
        private Timer readAndAirPPOSF;
        private Timer readAndAirSRVSPD;
        private List<BaseDataVariableState> axisVariables;
        private List<BaseDataVariableState> coordinateVariables;
        private List<BaseDataVariableState> speedVariables;
        private CultureInfo cultureForNumbers = new CultureInfo("en-US");
        
        #endregion
    }
}
