/* ========================================================================
 * Copyright (c) 2005-2013 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;
using Opc.Ua;
using Opc.Ua.Configuration;
using System.IO;


namespace Opc.Ua.Server.Controls
{
    /// <summary>
    /// The primary form displayed by the application.
    /// </summary>
    public partial class ServerForm : Form
    {

        private static bool axisCbChecked;
        public static bool AxisCbChecked
        {
            get { return axisCbChecked; }
            set { axisCbChecked = value; }
        }

        private static bool coordinatesCbChecked;
        public static bool CoordinatesCbChecked
        {
            get { return coordinatesCbChecked; }
            set { coordinatesCbChecked = value; }
        }

        private static bool speedCbChecked;
        public static bool SpeedCbChecked
        {
            get { return speedCbChecked; }
            set { speedCbChecked = value; }
        }

        private static bool miomCbChecked;
        public static bool MiomCbChecked
        {
            get { return miomCbChecked; }
            set { miomCbChecked = value; }
        }

        #region Constructors
        /// <summary>
        /// Creates an empty form.
        /// </summary>
        public ServerForm()
        {
            InitializeComponent();           
        }
        
        /// <summary>
        /// Creates a form which displays the status for a UA server.
        /// </summary>
        public ServerForm(StandardServer server, ApplicationConfiguration configuration)
        {
            InitializeComponent();

            m_server = server;
            m_configuration = configuration;
            this.ServerDiagnosticsCTRL.Initialize(m_server, m_configuration);

            TrayIcon.Text = this.Text = m_configuration.ApplicationName;

            ComponentResourceManager resources = new ComponentResourceManager(typeof(ServerForm));
            TrayIcon.Icon = (Icon)(resources.GetObject("App.ico"));
        }


        /// <summary>
        /// Creates a form which displays the status for a UA server.
        /// </summary>
        public ServerForm(ApplicationInstance application)
        {
            InitializeComponent();

            m_application = application;
            m_server = application.Server as StandardServer;
            m_configuration = application.ApplicationConfiguration;
            this.ServerDiagnosticsCTRL.Initialize(m_server, m_configuration);

            TrayIcon.Text = this.Text = m_configuration.ApplicationName;

            ComponentResourceManager resources = new ComponentResourceManager(typeof(ServerForm));
            TrayIcon.Icon = (Icon)(resources.GetObject("App.ico"));
        }
        #endregion

        #region Private Fields
        private ApplicationInstance m_application;
        private StandardServer m_server;
        private ApplicationConfiguration m_configuration;       
        #endregion

        #region Event Handlers
        private void ServerForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
            }
        }

        private void TrayIcon_DoubleClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void Server_ExitMI_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ServerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                m_server.Stop();
            }
            catch (Exception exception)
            {
                Utils.Trace(exception, "Error stopping server.");
            }
        }

        private void TrayIcon_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                TrayIcon.Text = String.Format(
                    "{0} [{1} {2:HH:mm:ss}]",
                    m_configuration.ApplicationName,
                    m_server.CurrentInstance.CurrentState,
                    DateTime.Now);
            }
            catch (Exception exception)
            {
                Utils.Trace(exception, "Error getting server status.");
            }
        }

        private void axisCoordinatesCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.axisCoordinatesCB.Checked)
            {
                axisCbChecked = true;
            }
            else
            {
                axisCbChecked = false;
            }
        }

        private void robotCoordinatesCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.robotCoordinatesCB.Checked)
            {
                coordinatesCbChecked = true;
            }
            else
            {
                coordinatesCbChecked = false;
            }
        }

        private void speedCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.speedCB.Checked)
            {
                speedCbChecked = true;
            }
            else
            {
                speedCbChecked = false;
            }
        }

        private void miomCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.miomCB.Checked)
            {
                miomCbChecked = true;
            }
            else
            {
                miomCbChecked = false;
            }
        }
        #endregion

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Quit the application", "OPC UA", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void contentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Diese Programm dient als ein TCP zu OPC-UA Gateway f�r die drei Mitsubishi Roboter. Als Vorlage wurde ein Beispielserverprogramm der OPCFoundation benutzt. \nErstellt von Reimer ");

        }
    }
}
