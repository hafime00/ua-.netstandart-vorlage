/* ========================================================================
 * Copyright (c) 2005-2013 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

using Opc.Ua.Server.Controls;

namespace Opc.Ua.Server.Controls
{
    partial class ServerForm 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.ServerDiagnosticsCTRL = new Opc.Ua.Server.Controls.ServerDiagnosticsCtrl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.axisCoordinatesCB = new System.Windows.Forms.CheckBox();
            this.groupBoxCheckBoxes = new System.Windows.Forms.GroupBox();
            this.miomCB = new System.Windows.Forms.CheckBox();
            this.speedCB = new System.Windows.Forms.CheckBox();
            this.robotCoordinatesCB = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.groupBoxCheckBoxes.SuspendLayout();
            this.SuspendLayout();
            // 
            // TrayIcon
            // 
            this.TrayIcon.Text = "TrayIcon";
            this.TrayIcon.Visible = true;
            this.TrayIcon.DoubleClick += new System.EventHandler(this.TrayIcon_DoubleClick);
            this.TrayIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseMove);
            // 
            // ServerDiagnosticsCTRL
            // 
            this.ServerDiagnosticsCTRL.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ServerDiagnosticsCTRL.Location = new System.Drawing.Point(0, 130);
            this.ServerDiagnosticsCTRL.Name = "ServerDiagnosticsCTRL";
            this.ServerDiagnosticsCTRL.Size = new System.Drawing.Size(739, 352);
            this.ServerDiagnosticsCTRL.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(739, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            this.contentsToolStripMenuItem.Click += new System.EventHandler(this.contentsToolStripMenuItem_Click);
            // 
            // axisCoordinatesCB
            // 
            this.axisCoordinatesCB.AutoSize = true;
            this.axisCoordinatesCB.Location = new System.Drawing.Point(6, 19);
            this.axisCoordinatesCB.Name = "axisCoordinatesCB";
            this.axisCoordinatesCB.Size = new System.Drawing.Size(103, 17);
            this.axisCoordinatesCB.TabIndex = 3;
            this.axisCoordinatesCB.Text = "Axis coordinates";
            this.axisCoordinatesCB.UseVisualStyleBackColor = true;
            this.axisCoordinatesCB.CheckedChanged += new System.EventHandler(this.axisCoordinatesCB_CheckedChanged);
            // 
            // groupBoxCheckBoxes
            // 
            this.groupBoxCheckBoxes.Controls.Add(this.miomCB);
            this.groupBoxCheckBoxes.Controls.Add(this.speedCB);
            this.groupBoxCheckBoxes.Controls.Add(this.robotCoordinatesCB);
            this.groupBoxCheckBoxes.Controls.Add(this.axisCoordinatesCB);
            this.groupBoxCheckBoxes.Location = new System.Drawing.Point(0, 27);
            this.groupBoxCheckBoxes.Name = "groupBoxCheckBoxes";
            this.groupBoxCheckBoxes.Size = new System.Drawing.Size(739, 97);
            this.groupBoxCheckBoxes.TabIndex = 4;
            this.groupBoxCheckBoxes.TabStop = false;
            this.groupBoxCheckBoxes.Text = "Checked informations will be published via OPC-UA";
            // 
            // miomCB
            // 
            this.miomCB.AutoSize = true;
            this.miomCB.Location = new System.Drawing.Point(167, 19);
            this.miomCB.Name = "miomCB";
            this.miomCB.Size = new System.Drawing.Size(55, 17);
            this.miomCB.TabIndex = 6;
            this.miomCB.Text = "MIOM";
            this.miomCB.UseVisualStyleBackColor = true;
            this.miomCB.CheckedChanged += new System.EventHandler(this.miomCB_CheckedChanged);
            // 
            // speedCB
            // 
            this.speedCB.AutoSize = true;
            this.speedCB.Location = new System.Drawing.Point(6, 66);
            this.speedCB.Name = "speedCB";
            this.speedCB.Size = new System.Drawing.Size(57, 17);
            this.speedCB.TabIndex = 5;
            this.speedCB.Text = "Speed";
            this.speedCB.UseVisualStyleBackColor = true;
            this.speedCB.CheckedChanged += new System.EventHandler(this.speedCB_CheckedChanged);
            // 
            // robotCoordinatesCB
            // 
            this.robotCoordinatesCB.AutoSize = true;
            this.robotCoordinatesCB.Location = new System.Drawing.Point(6, 43);
            this.robotCoordinatesCB.Name = "robotCoordinatesCB";
            this.robotCoordinatesCB.Size = new System.Drawing.Size(113, 17);
            this.robotCoordinatesCB.TabIndex = 4;
            this.robotCoordinatesCB.Text = "Robot coordinates";
            this.robotCoordinatesCB.UseVisualStyleBackColor = true;
            this.robotCoordinatesCB.CheckedChanged += new System.EventHandler(this.robotCoordinatesCB_CheckedChanged);
            // 
            // ServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 482);
            this.Controls.Add(this.groupBoxCheckBoxes);
            this.Controls.Add(this.ServerDiagnosticsCTRL);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ServerForm";
            this.Text = "Mitsubishi OPC-UA gateway assambly station";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ServerForm_FormClosed);
            this.Resize += new System.EventHandler(this.ServerForm_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBoxCheckBoxes.ResumeLayout(false);
            this.groupBoxCheckBoxes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ServerDiagnosticsCtrl ServerDiagnosticsCTRL;
        private System.Windows.Forms.NotifyIcon TrayIcon;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.CheckBox axisCoordinatesCB;
        private System.Windows.Forms.GroupBox groupBoxCheckBoxes;
        private System.Windows.Forms.CheckBox robotCoordinatesCB;
        private System.Windows.Forms.CheckBox speedCB;
        private System.Windows.Forms.CheckBox miomCB;
    }
}
